//! Header file tht connects the main file and the hanoitower.c file
/*!
* \file hanoitower.h
* \author Fernando Wasilewski
* \author Renan Carlos Rodrigues
* \since 02/10/2015
*
*  Operational System Used: Ubuntu 14.04
*/

#ifndef HANOITOWER_H
#define	HANOITOWER_H

// Types definitions

typedef struct Disco {
    int diameter;
} Disco; //!< Definition of the type Disco

typedef struct Nodo {
    Disco disk;
    struct Nodo *pNext;
} Nodo; //!< Definition of the type Nodo

typedef struct Stack {
    int numDisks;
    struct Nodo *pTop;
} Stack; //!< Definition of the type Stack

void openFile();
void push(struct Nodo *pNodo, struct Nodo **pInicio);
struct Nodo* pop(struct Nodo **pInicio);
void fillStacks();
void fillStacks();
void moveDisk();
void printDisk(int d);
void print(struct Nodo *p);
void printStack(Stack *p);
void printGame();
void menu();
void sort(int* array, int tam);
int compareVetor(int* aux, int* ord, int tam);
int testStack(Stack *p);
int endGame();
void desallocDisks();

#endif	/* HANOITOWER_H */

