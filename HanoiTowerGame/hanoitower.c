#include <stdio.h>
#include <stdlib.h>
#include "hanoitower.h"
#define TRUE 1
#define FALSE 0

//! C file that defines all the functions for the Hanoi Tower Game
/*!
* \file hanoitower.c
* \author Fernando Wasilewski
* \author Renan Carlos Rodrigues
* \since 02/10/2015
*
*  Operational System Used: Ubuntu 14.04
*/


//Global variables
Stack stacks[7];
int nStacks, nDisks = 0;
int vector[25];

//! This functions reads the size numbers from the Hanoi.txt file and save them into the vector variable.
/*!
* \fn void openFile()
* \return void
*/
void openFile() {

    //open file hanoi.txt
    FILE *fp;
    fp = fopen("Hanoi.txt", "r");
    if (fp == NULL) {

        printf("Error opening the Hanoi.txt file!");
        getchar();
        exit(1);
    }

    //scan file
    while ((!feof(fp)) && (nDisks < 25)) {

        fscanf(fp, "%d\n", &vector[nDisks]);
        nDisks++;
    }
    if (nDisks % 5 != 0) {
        printf("ERROR: You need a multiple of 5 number of disks to play the game");
        getchar();
        exit(0);
    }


    //close file
    fclose(fp);
}

//! The push function basically add a new node to the top of a stack.
/*!
* \fn void push(struct Nodo *pNodo, struct Nodo **pStart)
* \param pNodo is the node to be pushed
* \param pStart is the top of a stack where the node will be pushed
* \return void
*/
void push(struct Nodo *pNodo, struct Nodo **pStart) { //INSERT

    pNodo ->pNext = *pStart; // (*pNodo).pProximo = pInicio;
    *pStart = pNodo;
}

//! The pop function pops out the topper node of a stack.
/*!
* \fn struct Nodo* pop(struct Nodo **pStart)
* \param pStart is the top node of the stack
* \return top node of the stack
*/
struct Nodo* pop(struct Nodo **pStart) { 

    if (*pStart == NULL) {
        return NULL;
    } else {
        struct Nodo *pAux = *pStart;
        *pStart = pAux ->pNext;
        return pAux;
    }
}

//! This function defines the number of stacks on the game and fill them based on the disks gathered from the Hanoi.txt file.
/*!
* \fn void fillStacks()
* \return void
* \sa push()
*/
void fillStacks() {

    int i, j;

    //Initiating stacks
    nStacks = ((nDisks / 5) + 2);
    if ((nDisks % 5) > 0) nStacks++;

    for (i = 0; i < nStacks; i++) {

        stacks[i].pTop = NULL;
        stacks[i].numDisks = 0;
    }

    //Alocating disks and filling the stacks
    for (i = 0; i < nDisks; i++) {

        struct Nodo* pnodo = malloc(sizeof (struct Nodo));
        pnodo -> disk.diameter = vector[i];

        j = 0;
        while (stacks[j].numDisks == 5) {
            j++;
        }
        push(pnodo, &stacks[j].pTop);
        stacks[j].numDisks++;
    }
}

//! The moveDisk function reads the information provided by the user on the standard input and and moves the disk from a stack to another.
/*!
* \fn void moveDisk()
* \return void
* \sa pop()
* \sa push()
*/
void moveDisk() {

    int stackOrigin, stackDestiny;
    Nodo *nodoAux;

    printf("Choose the origin stack: ");
    scanf("%d", &stackOrigin);
    printf("Choose the destiny stack: ");
    scanf("%d", &stackDestiny);
    stackOrigin--;
    stackDestiny--;

    if ((nodoAux = pop(&(stacks[stackOrigin].pTop))) == NULL) {

        printf("Empty stack. Invalid movement\nPress any key to proceed\n");
        getchar();
        getchar();
    } else {



        if ((stacks[stackDestiny].pTop == NULL) ||
                (((nodoAux->disk.diameter) <= ((stacks[stackDestiny].pTop)->disk.diameter)) && (stacks[stackDestiny].numDisks < 5))) {

            push(nodoAux, &(stacks[stackDestiny].pTop));
            stacks[stackOrigin].numDisks--;
            stacks[stackDestiny].numDisks++;
            getchar();
            getchar();

        } else {

            push(nodoAux, &(stacks[stackOrigin].pTop));
            printf("Movement is not valid\n");
            getchar();
            getchar();
        }


    }
}

//! The printDisk function prints on the terminal the representation for the size of the input disk.
/*!
* \fn void printDisk(int d)
* \param d is the int number that defines the size of disk to be printed.
* \return void
*/
void printDisk(int d) {

    switch (d) {

        case 1:
        {

            printf("     0\n");
            break;
        }
        case 3:
        {

            printf("    000\n");
            break;
        }
        case 5:
        {

            printf("   00000\n");
            break;
        }
        case 7:
        {

            printf("  0000000\n");
            break;
        }
        case 9:
        {

            printf(" 000000000\n");
            break;
        }

        default:
        {

            printf("Diameter not permited. Options are 1, 3, 5, 7 ,9\n");
        }
    }
}

//! This function prints a disk on the terminal.
/*!
* \fn void print(struct Nodo *p)
* \param p is the node to be printed on the terminal
* \return void
* \sa printDisk()
*/
void print(struct Nodo *p) {

    if (p == NULL)
        printf("\nInvalid Pointer\n");
    else {
        printDisk(p->disk.diameter);
    }
}



//!The printStack function prints an entire stack by popping, printing and pushing back each node of the stack
/*!
* \fn void printStack(Stack *p)
* \param p is the stack to be printed
* \return void
* \sa pop()
* \sa push()
* \sa print()
*/
void printStack(Stack *p) {

    struct Nodo *pAux = NULL;

    struct Nodo *pnodo = pop(&(p->pTop));
    if (pnodo == NULL) {

        printf("     =\n");
        printf("     =\n");
        printf("     =\n");
        printf("     =\n");
        printf("     =\n");
    }
    while (pnodo != NULL) {

        print(pnodo);
        push(pnodo, &pAux);
        pnodo = pop(&(p->pTop));
    }

    printf("===========\n");
    pnodo = pop(&pAux);
    while (pnodo != NULL) {

        push(pnodo, &(p->pTop));
        pnodo = pop(&pAux);
    }
}

//! This function prints the game by printing each stack with their current disks
/*!
* \fn void printGame()
* \return void
* \sa printStack()
*/
void printGame() {

    int i;
    //printing the current status of the game
    for (i = 0; i < nStacks; i++) {

        printf("\n\n");
        printStack(&stacks[i]);
        printf("Stack %d\n", i + 1);
    }
}

//! This function prints the Game menu
/*!
* \fn void menu()
* \return void
*/
void menu() {

    printf("\nHanoi Tower\n");
    printf("1 - Move\n");
    printf("2 - Exit\n");
    printf("Choose an option: ");
}


//! This function implements a basic bubble sort algorithm that sorts an array based on a stack
/*!
* \fn void sort(int* array, int tam)
* \param array is an array of ints that represents the disks of a stack
* \param tam is the size of the array
* \return void
*/
void sort(int* array, int tam) {
    int swap = 0;
    int x = 0;
    for (x = 0; x < tam - 1; x++) {
        if (array[x] > array[x + 1]) {
            swap = array[x];
            array[x] = array[x + 1];
            array[x + 1] = swap;
        }
    }
}

//! This function verifies if an ordered array is equal compared to the array which represents the current formation of a stack
/*!
* \fn int compareVetor(int* aux, int* ord, int tam)
* \param aux is the array with the current formation of the stack
* \param ord is the sorted array to be compared with the real array
* \param tam is the size of the arrays (common between the arrays)
* \return 1 if the arrays are equals or 0 if differents.
*/
int compareVetor(int* aux, int* ord, int tam) {

    int j;
    for (j = 0; j < tam; j++) {

        if (aux[j] != ord[j]) return FALSE;
    }

    return TRUE;
}

//! The testStack function verifies if a stack is sorted or not.
/*!
* \fn int testStack(Stack *p)
* \param p is the stack to be verified about the sort condition
* \return 1 if the stack is sorted or 0 if it's not
* \sa sort()
* \sa compareVetor()
* \sa push()
* \sa pop()
*/
int testStack(Stack *p) {

    int vetorOrd[5], vetorAux[5], i = 0;
    Nodo *pAux = NULL;

    Nodo *pnodo = pop(&(p->pTop));

    while (pnodo != NULL) {

        vetorAux[i] = pnodo->disk.diameter;
        vetorOrd[i] = vetorAux[i];
        i++;
        push(pnodo, &pAux);
        pnodo = pop(&(p->pTop));
    }

    pnodo = pop(&pAux);
    while (pnodo != NULL) {

        push(pnodo, &(p->pTop));
        pnodo = pop(&pAux);
    }

    sort(vetorOrd, i);
    return compareVetor(vetorAux, vetorOrd, i);

}

//! This function verifies the current status of the game. If all the filled stacks has their disks sorted, and all the 5 disks, than the game is done.
/*!
* \fn int endGame() 
* \return 1 if the game is finished or 0 if it's not
* \sa testStack()
*/
int endGame() {

    int i;

    for (i = 0; i < nStacks; i++) {

        if ((testStack(&stacks[i])) == 0) {
            return FALSE;
        };
        if (stacks[i].numDisks != 5 && stacks[i].pTop != NULL) {
            return FALSE;
        }
    }

    return TRUE;
}

//!This functions frees the memory allocated for the disks
/*!
* \fn void desallocDisks()
* \return void
*/
void desallocDisks() {
    int x = 0;
    for (x = 0; x < nStacks; x++) {
        if (stacks[x].pTop != NULL) {
            Nodo *pnodo = stacks[x].pTop;
            while (pnodo->pNext != NULL) {
                Nodo *pAux = pnodo;
                pnodo = pnodo->pNext;
                free(pAux);
            }
        }
    }
}
