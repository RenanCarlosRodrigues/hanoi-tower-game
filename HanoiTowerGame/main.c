#include <stdio.h>
#include <stdlib.h>
#include "hanoitower.h"
#define TRUE 1
#define FALSE 0
#define MOVE 1
#define EXIT 2


//! Main file of the Hanoi Tower Game
/*!
* \file main.c
* \author Fernando Wasilewski
* \author Renan Carlos Rodrigues
* \since 02/10/2015
*
*  Operational System Used: Ubuntu 14.04
*/


//! Main function that controls the Hanoi Tower Game
/*!
* \fn int main()
* \return 0
*/
int main() {
    int option = 1;

    openFile();
    fillStacks();
    printf("\n#### WELCOME TO THE HANOI TOWER GAME ####");
    //menu loop
    while (option != EXIT) {
        printGame();
        menu();
        scanf("%d", &option);
        if (option == MOVE) {
            moveDisk();
            if (endGame() == TRUE) {
                printf("YOU FINISHED THE GAME!\n");
                printGame();
                desallocDisks();
                option = EXIT;
            }
        }
        system("clear");
    }

    printf("\nPress any key to exit the game\n");
    getchar();
    return 0;

}
