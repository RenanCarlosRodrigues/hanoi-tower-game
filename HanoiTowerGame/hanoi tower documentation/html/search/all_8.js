var searchData=
[
  ['pop',['pop',['../hanoitower_8c.html#ae13ed9cf7d2f12c4f951b3c3f7c114e0',1,'pop(struct Nodo **pStart):&#160;hanoitower.c'],['../hanoitower_8h.html#a6ab0128deca7ccdf8dc4295d5679ee38',1,'pop(struct Nodo **pInicio):&#160;hanoitower.c']]],
  ['print',['print',['../hanoitower_8c.html#a62fcb4a6f0dee93f1c1cc1097e397355',1,'print(struct Nodo *p):&#160;hanoitower.c'],['../hanoitower_8h.html#a62fcb4a6f0dee93f1c1cc1097e397355',1,'print(struct Nodo *p):&#160;hanoitower.c']]],
  ['printdisk',['printDisk',['../hanoitower_8c.html#abc796b8b5f62d739bbdf728acf36702a',1,'printDisk(int d):&#160;hanoitower.c'],['../hanoitower_8h.html#abc796b8b5f62d739bbdf728acf36702a',1,'printDisk(int d):&#160;hanoitower.c']]],
  ['printgame',['printGame',['../hanoitower_8c.html#a588696d89f65cab797ac3aa09db8ace5',1,'printGame():&#160;hanoitower.c'],['../hanoitower_8h.html#a588696d89f65cab797ac3aa09db8ace5',1,'printGame():&#160;hanoitower.c']]],
  ['printstack',['printStack',['../hanoitower_8c.html#a87437e4f31ba7f79ad030531757a2edf',1,'printStack(Stack *p):&#160;hanoitower.c'],['../hanoitower_8h.html#a87437e4f31ba7f79ad030531757a2edf',1,'printStack(Stack *p):&#160;hanoitower.c']]],
  ['push',['push',['../hanoitower_8c.html#a426e7517d8533d291b81f6b54d0302a4',1,'push(struct Nodo *pNodo, struct Nodo **pStart):&#160;hanoitower.c'],['../hanoitower_8h.html#a643feaecc2798708a286f5ee8736dc3d',1,'push(struct Nodo *pNodo, struct Nodo **pInicio):&#160;hanoitower.c']]]
];
